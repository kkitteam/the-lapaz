<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Tumblr\API\Client;

/**
 * @Route("/")
 */
class PostController extends Controller
{

    const BLOG_NAME = 'calyja';
    const LIMIT = 50;

    /**
     * 
     * @return int
     */
    private function getLimit()
    {
        return self::LIMIT;
    }

    /**
     * 
     * @param int $page
     * @return int
     */
    private function getOffset( $page )
    {
        return $this->getLimit() * $page;
    }

    /**
     * 
     * @return Client
     */
    private function getTumblrClient()
    {
        return $this->container->get('ushios_tumblr_client.default');
    }

    /**
     * @Route("/", name="post_list")
     */
    public function listAction()
    {
        $client = $this->getTumblrClient();

        $postsParameters = array(
            'type' => 'photo',
            'limit' => $this->getLimit()
        );

        $lefPosts = $client->getBlogPosts(self::BLOG_NAME, array_merge($postsParameters, array(
                'offset' => $this->getOffset(1))))->posts;
        $centerPosts = $client->getBlogPosts(self::BLOG_NAME, array_merge($postsParameters, array(
                'offset' => $this->getOffset(2))))->posts;
        $rightPosts = $client->getBlogPosts(self::BLOG_NAME, array_merge($postsParameters, array(
                'offset' => $this->getOffset(3))))->posts;

        return $this->render("post/list.html.twig", array(
                'leftPosts' => $lefPosts,
                'centerPosts' => $centerPosts,
                'rightPosts' => $rightPosts
        ));
    }

    /**
     * 
     * @param Request $request
     * @Route("/post/show", name="post_show", options={"expose" = true})
     * @Method({"POST"})
     */
    public function ajaxPostAction( Request $request )
    {

        // get form values
        $data = json_decode($request->getContent());
        
        $id = $data->id;
        $client = $this->getTumblrClient();

        $postParameters = array(
            'id' => $id,
            'limit' => 1
        );

        $posts = $client->getBlogPosts(self::BLOG_NAME, $postParameters);

        if ( is_array($posts) && !empty($posts) )
        {
            $post = $posts[0]->posts[0];
        }
        else
        {
            $post = $posts->posts[0];
        }
        
        $view = $this->render("post/post.html.twig", array(
            'post' => $post,
        ));

        $response = new JsonResponse();
        $response->setData(array(
            'view' => trim(preg_replace('/\s+/', ' ', $view))
        ));
        return $response;
    }

    /**
     * 
     * @param Request $request
     * @Route("/post/sublist", name="post_sublist", options={"expose" = true})
     * @Method({"POST"})
     */
    public function ajaxSublistAction( Request $request )
    {
        // get form values
        $data = json_decode($request->getContent());
        $page = $data->page;
        $client = $this->getTumblrClient();

        $postParameters = array(
            'offset' => $this->getOffset($page),
            'limit' => $this->getLimit()
        );

        $posts = $client->getBlogPosts(self::BLOG_NAME, $postParameters);

        $view = $this->render('post/sublist.html.twig', array(
            'posts' => $posts,
        ));

        $response = new JsonResponse();
        $response->setData(array(
            'view' => trim(preg_replace('/\s+/', ' ', $view))
        ));
        return $response;
    }

}
