var toArray = function (data) {
    return $(data).serializeArray();
};

var toJson = function (data) {
    return JSON.stringify(data);
};

var myForm = function (objForm, objLoad) {
    $.ajax({
        url: objForm['loadWhere'],
        data: JSON.stringify(objLoad),
        dataType: 'json',
        type: (objForm['type'] ? objForm['type'] : 'POST'),
        beforeSend: function () {
            if (typeof (objForm.beforeSend) == 'function') {
                objForm.beforeSend.call();
            }
        },
        success: function (data, status) {
            var output = '';
            if (objForm.view == 'html') {
                output = data.view;
            } else {
                output = data;
            }

            if (objForm.loadPrepend) {
                $(objForm.loadPrepend).prepend(output);
            } else if (objForm.loadAppend) {
                $(objForm.loadAppend).append(output);
            } else if (objForm.loadAfter) {
                $(objForm.loadAfter).after(output);
            } else {
                $(objForm.loadTo).html(output);
            }

            if (typeof (objForm.afterSuccess) == 'function') {
                objForm.afterSuccess.call();
            }
        }
    });
};

$(document).ready(function () {
    $('body').on('click', '.post', function () {
        var objForm = {
            loadTo: '#post',
            view: 'html',
            loadWhere: Routing.generate('post_show'),
            afterSuccess: function () {
                $("#list").css({
                    top: '100%'
                });
                $("#post").css({
                    top: 0
                });
            }
        }, objLoad = {
            'id': $(this).attr('id')
        };
        myForm(objForm, objLoad);
        return false;
    });
    
    $('body').on('click', '#next', function () {
        var leftList = $('.sublist.left');
        var centerList = $('.sublist.center');
        var rightList = $('.sublist.right');

        leftList.css('transition', '0s');
        centerList.css('transition', '1s');
        rightList.css('transition', '1s');
        
        centerList.removeClass('center').addClass('left');
        rightList.removeClass('right').addClass('center');
        leftList.removeClass('left').addClass('right');
    });
    
    $('body').on('click', '#previous', function () {
        var leftList = $('.sublist.left');
        var centerList = $('.sublist.center');
        var rightList = $('.sublist.right');
        
        rightList.css('transition', '0s');
        centerList.css('transition', '1s');
        leftList.css('transition', '1s');
        
        centerList.removeClass('center').addClass('right');
        leftList.removeClass('left').addClass('center');
        rightList.removeClass('right').addClass('left');
    });
    
});